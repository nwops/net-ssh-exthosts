# frozen_string_literal: true

require 'net/ssh/exthosts/version'

module Net
  module Ssh
    class Exthosts < ::Net::SSH::KnownHosts
      class <<self
        # Searches all known host files (see KnownHosts.hostfiles) for all keys
        # of the given host. Returns an enumerable of keys found.
        def search_for(host, options = {})
          HostKeys.new(search_in(hostfiles(options), host, options), host, self, options)
        end

        # Search for all known keys for the given host, in every file given in
        # the +files+ array. Returns the list of keys.
        def search_in(files, host, options = {})
          files.flat_map { |file| KnownHosts.new(file).keys_for(host, options) }
        end

        # A centrally controlled system should not reference a local host file
        def hostfiles(_options, _which = :all)
          []
        end

        # Only a central authority can add a host
        def add(_host, _key, _options = {})
          true
        end
      end

      # The host-key file name that this KnownHosts instance will use to search
      # for keys.
      attr_reader :callback

      # Instantiate a new KnownHosts instance that will search the given known-hosts
      # file. The path is expanded file File.expand_path.
      # @param [String] - the callback command to execute
      def initialize(source)
        @callback = source.to_s
      end

      def render_call(text, host, input, output)
        text.gsub('%h', host)
        .gsub('%fd_in', input.fileno)
        .gsub('%fd_out', output.fileno)
      end

      # Returns an array of all keys that are known to be associatd with the
      # given host. The +host+ parameter is either the domain name or ip address
      # of the host, or both (comma-separated). Additionally, if a non-standard
      # port is being used, it may be specified by putting the host (or ip, or
      # both) in square brackets, and appending the port outside the brackets
      # after a colon. Possible formats for +host+, then, are;
      #
      #   "net.ssh.test"
      #   "1.2.3.4"
      #   "net.ssh.test,1.2.3.4"
      #   "[net.ssh.test]:5555"
      #   "[1,2,3,4]:5555"
      #   "[net.ssh.test]:5555,[1.2.3.4]:5555
      def keys_for(host, options = {})
        keys = []
        return keys if callback.empty?

        entries = host.split(/,/)
        host_name = entries[0]
        host_ip = entries[1]
        # /opt/boksm/lib/boks_getsslkey -i 4 -o 7
        input = TmpFile.new('known_hosts_in')
        output = TmpFile.new('known_hosts_out')
        render_call(source, host, input, output)
        # command <input> <output>
        data = `#{callback}`
        File.open(source) do |file|
          file.each_line do |line|
            hosts, type, key_content = line.split(' ')
            next unless hosts || hosts.start_with?('#')

            hostlist = hosts.split(',')

            next unless SUPPORTED_TYPE.include?(type)

            found = hostlist.any? { |pattern| match(host_name, pattern) } || known_host_hash?(hostlist, entries)
            next unless found

            found = hostlist.include?(host_ip) if options[:check_host_ip] && entries.size > 1 && hostlist.size > 1
            next unless found

            blob = key_content.unpack1('m*')
            keys << Net::SSH::Buffer.new(blob).read_key
          end
        end

        keys
      end

      def match(host, pattern)
        if pattern.include?('*') || pattern.include?('?')
          # see man 8 sshd for pattern details
          pattern_regexp = pattern.split('*').map do |x|
            x.split('?').map do |y|
              Regexp.escape(y)
            end.join('.')
          end.join('[^.]*')

          host =~ Regexp.new("\\A#{pattern_regexp}\\z")
        else
          host == pattern
        end
      end

      # Indicates whether one of the entries matches an hostname that has been
      # stored as a HMAC-SHA1 hash in the known hosts.
      def known_host_hash?(hostlist, entries)
        if hostlist.size == 1 && hostlist.first =~ /\A\|1(\|.+){2}\z/
          chunks = hostlist.first.split(/\|/)
          salt = Base64.decode64(chunks[2])
          digest = OpenSSL::Digest.new('sha1')
          entries.each do |entry|
            hmac = OpenSSL::HMAC.digest(digest, salt, entry)
            return true if Base64.encode64(hmac).chomp == chunks[3]
          end
        end
        false
      end

      # Since keys are controlled via a central source it is not possible to add to the local source
      def add(_host, _key)
        true
      end
    end
  end
end
