module Net
  module Ssh
    class BoksKnownHosts < ::Net::SSH::ExtHosts
      SOURCE = '/opt/boksm/lib/boks_getsslkey -i %in_fd -o %out_fd'

      def initialize(source = SOURCE)
        super
      end
    end
  end
end
