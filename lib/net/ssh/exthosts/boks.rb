module Net
  module Ssh
    class BoksKnownHosts < ::Net::SSH::ExtHosts
      SOURCE = '/opt/boksm/lib/boks_getsslkey -i %fd_in -o %fd_out'

      def initialize(source = SOURCE)
        super
      end
    end
  end
end
