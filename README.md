# External Host Keys for Ruby Net::SSH

The net-ssh-exthosts library is a gem created to allow the ruby NET::SSH library to lookup host keys
via a callback command.  This is especially useful for storing host keys in a central location.  It is designed
to work with any kind of central location as the user supplies the command to retrieve the database or perform the lookup directly. 

The original problem has been described in this [ticket](https://github.com/net-ssh/net-ssh/issues/732).

```
We use an alternative ssh client that provides centralized management of ssh host keys and authorization. Due to the central management feature of my ssh client, the ssh client does not write public host keys to the known hosts file and instead runs a command to consult a database of known hosts to verify the remote host during the ssh connection process. I expect Net::SSH to be able to validate the remote host key with something other than a known hosts file. Having the ability to specify a command to run to produce the remote host key would be ideal.
```

This is currently a WIP / design phase.  We are still collecting usage details in order to better design and implement this gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'net-ssh-exthosts'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install net-ssh-exthosts

## Usage

Set ExtHostCommand in your .ssh/config file. 

```
  ExtHostCommand "curl https://companyx.com/data/ssh_host_keys.txt"

```

or

```
  ExtHostCommand "curl https://companyx.com/data/ssh_host_keys/%h.txt"

```

or

```
  ExtHostCommand "/opt/boks/lib/gethost -i %fd_in -o %fd_out"

```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/logicminds/net-ssh-exthosts. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/logicminds/net-ssh-exthosts/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Net::Ssh::Exthosts project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/logicminds/net-ssh-exthosts/blob/master/CODE_OF_CONDUCT.md).
